import { Component, OnInit,Input } from '@angular/core';
import { AppService } from 'src/app/service/app.service';
import { Rutas} from '../service/interface/interface-class.model';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-novedades',
  templateUrl: './novedades.component.html',
  styleUrls: ['./novedades.component.css'],
  providers: [DatePipe]
})
export class NovedadesComponent implements OnInit {

  idEmpresal: any;
  dato: any;
  datos:any;
  rutas:Array<Rutas>;
  @Input() idEmpresa: string;
  idPasajero: string;
  fechaInicio: string;
  fechaFin: string;
  fechaactual: any;
  fechaIniciod: any;
  fechaFind: any;
  fechaactuald: any;
  motivos = [
    {motivodetalle: 'Teletrabajo'},
    {motivodetalle: 'Vacaciones'},
    {motivodetalle: 'Permisos'},
    {motivodetalle: 'Desistimiento de la ruta'}
  ];

  anios = [
    {an:'2020'},
    {an:'2021'}
  ]
  meses = [
    {m:'1'},
    {m:'2'},
    {m:'3'},
    {m:'4'},
    {m:'5'},
    {m:'6'},
    {m:'7'},
    {m:'8'},
    {m:'9'},
    {m:'10'},
    {m:'11'},
    {m:'12'},
  ]

  dias = [
    {d:'1'},
    {d:'2'},
    {d:'3'},
    {d:'4'},
    {d:'5'},
    {d:'6'},
    {d:'7'},
    {d:'8'},
    {d:'9'},
    {d:'10'},
    {d:'11'},
    {d:'12'},
    {d:'13'},
    {d:'14'},
    {d:'15'},
    {d:'16'},
    {d:'17'},
    {d:'18'},
    {d:'19'},
    {d:'20'},
    {d:'21'},
    {d:'22'},
    {d:'23'},
    {d:'24'},
    {d:'25'},
    {d:'26'},
    {d:'27'},
    {d:'28'},
    {d:'29'},
    {d:'30'},
    {d:'31'}
  ]
  
  addressForm2 = this.fb.group({
    ruta: [null, Validators.required],
   motivo: [null, Validators.required],
  dia: [null, Validators.required],
   mes: [null, Validators.required],
   anio: [null, Validators.required],
   dia2: [null, Validators.required],
   mes2: [null, Validators.required],
   anio2: [null, Validators.required]
    });
  constructor(private fb: FormBuilder,private service: AppService,private datePipe: DatePipe) { }

  ngOnInit(){
    this.idPasajero = JSON.parse(localStorage.getItem('Datos usuario')).idPasajero;
    this.idEmpresa = JSON.parse(localStorage.getItem('Datos usuario')).idEmpresa;
    this.rutas = new Array<Rutas>();
    this.rutasempresa();
    this.fechaactual = new Date();
  }

  rutasempresa(){
    this.service.postRutasempresa(this.idEmpresa)
    .subscribe(dato=>{
      if(dato.error != 1){
        this.rutas = dato.response;
        console.log(this.rutas)
          }else{
            alert(dato.response);
          }  
    }
    )
      }

      AgregarNovedad(){
        this.fechaInicio = this.addressForm2.get('anio').value.toString()+"-"+this.addressForm2.get('mes').value.toString()+"-"+this.addressForm2.get('dia').value.toString()
        this.fechaFin = this.addressForm2.get('anio2').value.toString()+"-"+this.addressForm2.get('mes2').value.toString()+"-"+this.addressForm2.get('dia2').value.toString()
        this.fechaIniciod =  new Date(this.fechaInicio);
        this.fechaFind =  new Date(this.fechaFin);
        console.log(this.fechaIniciod)
        console.log(this.fechaFind)
        if(this.fechaFin > this.fechaInicio || this.fechaIniciod > this.fechaactuald){
          this.service.postNovedades(this.addressForm2.get('ruta').value.toString(),this.idPasajero,this.addressForm2.get('motivo').value.toString(),this.fechaInicio, this.fechaFin)
          .subscribe(datos=>{
            if(datos.error != 1){
              document.getElementById('modal').style.display = 'block';
              console.log(datos.response) 
            }else{
              // console.log(datas) 
              alert(datos.response);
            }    
          });
        }else{
          alert("La fecha final debe se mayor");
        }        
      }

}
