import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, tap, map, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  api = 'https://api.rutapps.co/index.php/web/v4/pasajero/';
  
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods" : "GET, PUT, POST, DELETE, HEAD"
    })
  };

  constructor(private http: HttpClient) { }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

  post(url, data): Observable<any> {
    const api = `${this.api}${url}`;
    const param = {data: data};
    return this.http.post(api, param, this.httpOptions)
    .pipe(
      catchError(this.errorHandl)
      // retry(3)
    );
  }

  postRegister(idEmpresa:string, nombre: string, telefono: string, tipoDocumento: string, documento: string,clave: string): Observable<any> {
    let data = {
        "data":{
          "idEmpresa": idEmpresa,
          "nombre": nombre,
          "telefono": telefono,
          "tipoDocumento": tipoDocumento,
          "documento": documento,
          "clave": clave
        }
    }
    return this.http.post('https://api.rutapps.co/index.php/web/v5/pasajero/registro.json/', data)
    .pipe(
      map((res) => res as any)
    )
  }

  postLogin(usuario: string, clave: string): Observable<any>{
    let data = {
      "data":{
        "usuario": usuario,
        "clave": clave
      }
  }
  return this.http.post('https://api.rutapps.co/index.php/web/v5/pasajero/login.json/', data)
  .pipe(
    map((res) => res as any)
  )
  }
 
  postRutasempresa(idEmpresa: string):Observable<any>{
    let data = {
      "data":{
        "idEmpresa": idEmpresa
      }
  }
  return this.http.post('https://api.rutapps.co/index.php/web/v5/pasajero/rutas.json/', data)
  .pipe(
    map((res) => res as any)
  )
  }

  postMisrutas(idPasajero:string):Observable<any>{
    let data = {
      "data":{
        "idPasajero": idPasajero
      }
  }
  return this.http.post('https://api.rutapps.co/index.php/web/v5/pasajero/misrutas.json/', data)
  .pipe(
    map((res) => res as any)
  )
  }
  
  postRegistroRuta(idPasajero:string, idRuta:string):Observable<any>{
    let data = {
      "data":{
        "idPasajero": idPasajero,
        "idRuta": idRuta
      }
  }
  return this.http.post('https://api.rutapps.co/index.php/web/v5/pasajero/registrarruta.json/', data)
  .pipe(
    map((res) => res as any)
  )
  }

  postCheck(idPasajero:string,idRuta:string  ):Observable<any>{
    let data = {
      "data":{
        "idPasajero": idPasajero,
        "idRuta": idRuta    
      }
  }
  return this.http.post('https://api.rutapps.co/index.php/web/v5/pasajero/checking.json', data)
  .pipe(
    map((res) => res as any)
  )
  }

  postNovedades(idRuta:string, idPasajero:string, motivo: string,fechaInicio:string, fechaFin:string):Observable<any>{
    let data = {
      "data":{
        "idRuta": idRuta,
        "idPasajero": idPasajero,
        "motivo": motivo,
        "fechaInicio": fechaInicio,
        "fechaFin": fechaFin,
      }
  }
  return this.http.post('https://api.rutapps.co/index.php/web/v5/pasajero/novedades.json/', data)
  .pipe(
    map((res) => res as any)
  )
  }
  
 

}