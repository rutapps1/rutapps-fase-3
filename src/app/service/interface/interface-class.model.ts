export class interfaceFoRecover { 
    error?:number
    response?:Rutas
}

export class usuarioResponse { 
    error?:number
    response?:Array<usuarioRuta>
}

export class rutasusuario{
    eliminado?: string
idEmpresa?: string
idPasajero?: string
idRelacion?: string
idRuta?: string
reg?: string
}

export class responseFoRecover{
    nombre?: string
    telefono?: string
    tipoDocumento?: string
    documento?: string
    idEmpresa?: string
    clave?:string
}

export class Rutas{
    codigo?:string
    conductor?: conductor
    eliminado?: string
    empresa?: empresa
    idConductor?: string
    idEmpresa?:string
    idRuta?:string
    idVehiculo?:string
    nombre?: string
    reg?: string
    vehiculo?: vehiculo
}

export class empresa{
    contacto?: string
    correo?: string
    direccion?: string
    eliminado?: string
    idEmpresa?: string
    lat?: string
    lng?:string
    nit?:string
    nombre?:string
    reg?:string
    telefono?: string
}

export class conductor{
    clave?:string
    documento?: string
    eliminado?: string
    idConductor?: string
    idEmpresa?: string
    nombre?:string
    reg?: string
    telefono?: string
    usuario?: string    
}

export class vehiculo{
    clase?:string 
    codigoInterno?:string 
    descripcion?:string 
    documento?: string 
    eliminado?:string 
    idEmpresa?: string 
    idVehiculo?:string 
    marca?: string 
    modelo?: string 
    nombrePropietario?: string 
    placa?: string 
    reg?: string 
    tarjetaOperacion?: string 
}

export class usuarioRuta{
    idHistorial?: string
    idEmpresa?: string
    idRuta?: string
    nombre?: string
    documento?: string
    telefono?: string
    tipoDocumento?: string
    reg?: string
    eliminado?: string
}

