import { Component, Input, OnInit} from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppService } from 'src/app/service/app.service'
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { Rutas, interfaceFoRecover } from '../service/interface/interface-class.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  addressForm = this.fb.group({
      usuario: [null, Validators.required],
      clave: [null, Validators.required],
    });

  @BlockUI() blockUI: NgBlockUI;
  
 cargando: boolean;
 defaultData: interfaceFoRecover; 
 data : interfaceFoRecover;
 datas : any;
@Input() idEmpresa: string;
  constructor(private fb: FormBuilder,private _route: ActivatedRoute, private router: Router, private service: AppService, private http: HttpClient) {
    this.cargando = false;
   }

  ngOnInit(){

  }

  backPage(url) {
    this.router.navigateByUrl(url)
  }


  Login(){
    this.blockUI.start('Cargando');
    this.service.postLogin(this.addressForm.get('usuario').value.toString(),this.addressForm.get('clave').value)
    .subscribe(datas=>{
      this.blockUI.stop();
      console.log(datas)
      if(datas.response.usuario == this.addressForm.get('usuario').value.toString() && datas.response.clave == this.addressForm.get('clave').value){
        localStorage.setItem('Datos usuario',JSON.stringify(datas.response));
        this.router.navigateByUrl("/Home3Component")
      }else{
  
        alert("El usuario o contraseña son incorrectos");
      }
    })
  }
    

}
