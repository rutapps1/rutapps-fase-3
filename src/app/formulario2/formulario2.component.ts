import { Component, Input, OnInit} from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppService } from 'src/app/service/app.service'
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { Rutas, interfaceFoRecover } from '../service/interface/interface-class.model';

@Component({
  selector: 'app-formulario2',
  templateUrl: './formulario2.component.html',
  styleUrls: ['./formulario2.component.css']
})

export class Formulario2Component implements OnInit {
  addressForm = this.fb.group({
    idEmpresa: [null, Validators.required],
  nombre: [null, Validators.required],
    telefono: [null, Validators.required],
    tipoDocumento: [null, Validators.required],
    documento: [null, Validators.required],
    clave: [null, Validators.required],
    password2: [null, Validators.required],
    city: [null, Validators.required]
  });

  hasUnitNumber = false;

  states = [
    {name: 'Cédula de ciudadania', abbreviation: 'Cédula de ciudadania'},
    {name: 'Cédula de extrangeria', abbreviation: 'Cédula de extrangeria'},
    {name: 'Pasaporte', abbreviation: 'Pasaporte'},
  ];

  @BlockUI() blockUI: NgBlockUI;
  
  selected ='Cedula de ciudadania'; 
check: boolean;
  button = 'Submit';
  isLoading = false;
  cargando: boolean;
  defaultData: interfaceFoRecover; 
 data : interfaceFoRecover;
 datas : any;
// @Input() idEmpresa: string;


  constructor(private fb: FormBuilder,private _route: ActivatedRoute, private router: Router, private service: AppService, private http: HttpClient) {
    this.cargando = false;
   }
 
  ngOnInit(){
    // // this.idEmpresa = this._route.snapshot.paramMap.get("idEmpresa");
    // this.idEmpresa ="1"
  }

  backPage(url) {
    this.router.navigateByUrl(url)
  }

registrar() {
  this.isLoading = true;
  this.button = 'Enviando';
  setTimeout(() => {
    this.isLoading = false;
    this.button = 'Submit';
  }, 8000)
    this.blockUI.start('Cargando');
    if  (this.addressForm.get('nombre').value == null || this.addressForm.get('nombre').value == null || this.addressForm.get('telefono').value == null 
    || this.addressForm.get('tipoDocumento').value == null || this.addressForm.get('documento').value == null
    || this.addressForm.get('clave').value == null)
    {
     console.log("Hay datos faltantes");
   }  
   else {
    if(this.addressForm.get('clave').value == this.addressForm.get('password2').value || this.addressForm.get('password2').value == this.addressForm.get('clave').value ){
      this.service.postRegister(this.addressForm.get('idEmpresa').value.toString(),this.addressForm.get('nombre').value.toString(),this.addressForm.get('telefono').value.toString(),this.addressForm.get('tipoDocumento').value.toString(),this.addressForm.get('documento').value.toString(),this.addressForm.get('clave').value.toString())
      .subscribe(datas=>{
       this.blockUI.stop();
       if(datas.error != 1){
        localStorage.setItem('Datos usuario',JSON.stringify(datas.response));
        document.getElementById('modal').style.display = 'block';
      }else{
        // console.log(datas) 
        alert(datas.response);
      } 
      })
    }else{
alert("la contraseña y confirmacion no coinciden");
    }
   }
    };
  }

  


