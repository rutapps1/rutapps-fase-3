import { Component, Input, OnInit} from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppService } from 'src/app/service/app.service'
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { Rutas, interfaceFoRecover } from '../service/interface/interface-class.model';

@Component({
  selector: 'app-gracias',
  templateUrl: './gracias.component.html',
  styleUrls: ['./gracias.component.css']
})
export class GraciasComponent implements OnInit {
  idRuta: Array<Rutas>;
  idPasajero: string;
  dato: any;
  
  constructor(private _route: ActivatedRoute, private router: Router, private service: AppService, private http: HttpClient) { }

  ngOnInit() {
    this.idRuta = JSON.parse(localStorage.getItem('Datos ruta 2')) as Array<Rutas>;
    this.idPasajero = JSON.parse(localStorage.getItem('Datos usuario')).idPasajero;
  console.log("idruta s",JSON.stringify(this.idRuta))
  console.log("idpasajero",JSON.stringify(this.idPasajero))
  }

  rutausuario(){
    this.service.postRegistroRuta(this.idPasajero,this.idRuta[0].idRuta)
    .subscribe(dato=>{
      console.log("vista",dato.response)
    })
  }
}
