import { Component, Input, OnInit} from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/service/app.service'
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { usuarioResponse, usuarioRuta } from '../service/interface/interface-class.model';

@Component({
  selector: 'app-pasajero',
  templateUrl: './pasajero.component.html',
  styleUrls: ['./pasajero.component.css']
})
export class PasajeroComponent implements OnInit {

  cargando:boolean;
  defaultResponse: usuarioResponse; 
  dataUsuarios : usuarioResponse;
  datas : any;
 @Input() idRuta: string; 

  constructor(private _route: ActivatedRoute,private router: Router, private service: AppService, private http: HttpClient) {
    this.cargando = false;
  }

  ngOnInit(): void {
    this.dataUsuarios = new usuarioResponse();
    this.idRuta = this._route.snapshot.paramMap.get("idRuta");
  // this.getListaUsuarios();
  this.defaultResponse = {
    error:0,
    response:[{documento:"",
    eliminado:"",
    idEmpresa:"",
    idHistorial:"",
    idRuta:"",
    nombre:"",
    reg:"no hay informacion de pasajeros",
    telefono:"",
    tipoDocumento:""}]}
  }

  // getListaUsuarios(){
  //   this.cargando = true;
  //   this.service.GetListData(this.idRuta)
  //   .subscribe(dataResponse =>{
     
  //     if(dataResponse.error!=0){
  //       this.cargando = false;
  //       alert("El id de la ruta no existe")
  //       console.log("d",this.dataUsuarios);
  //       this.dataUsuarios=this.defaultResponse;
  //     }else{
  //       this.dataUsuarios = dataResponse;
  //       this.cargando = false;
  //     }
  //   })
  // }
  

}
