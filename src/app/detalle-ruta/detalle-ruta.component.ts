import { Component, OnInit, Input } from '@angular/core';
import { AppService } from 'src/app/service/app.service'
import { Rutas } from '../service/interface/interface-class.model';

@Component({
  selector: 'app-detalle-ruta',
  templateUrl: './detalle-ruta.component.html',
  styleUrls: ['./detalle-ruta.component.css']
})
export class DetalleRutaComponent implements OnInit {
  idRuta: Array<Rutas>;
  idPasajero: string;
  dato: any;
  
  constructor(private service: AppService) { }

  ngOnInit(){
    this.idRuta = JSON.parse(localStorage.getItem('Datos ruta')) as Array<Rutas>;
    this.idPasajero = JSON.parse(localStorage.getItem('Datos usuario')).idPasajero;
  console.log("idruta s",JSON.stringify(this.idRuta))
  console.log("idpasajero",JSON.stringify(this.idPasajero))
  }

  rutausuario(){
    this.service.postRegistroRuta(this.idPasajero,this.idRuta[0].idRuta)
    .subscribe(dato=>{
      console.log("vista: 1",dato.response)
    })
  }

}
