import { Component, Input, OnInit} from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { AppService } from 'src/app/service/app.service'
import { Rutas } from '../service/interface/interface-class.model';

@Component({
  selector: 'app-formulario1',
  templateUrl: './formulario1.component.html',
  styleUrls: ['./formulario1.component.css']
})

export class Formulario1Component implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  
  idRuta: Array<Rutas>;
  idPasajero: string;
  dato: any;
  
  constructor(private service: AppService) {

   }
   
   ngOnInit(){
    this.idRuta = JSON.parse(localStorage.getItem('Datos ruta 2')) as Array<Rutas>;
    this.idPasajero = JSON.parse(localStorage.getItem('Datos usuario')).idPasajero;
  console.log("idruta completa:",JSON.stringify(this.idRuta))
  console.log("idpasajero",JSON.stringify(this.idPasajero))
  }

rutausuario(){
  this.service.postRegistroRuta(this.idPasajero,this.idRuta[0].idRuta)
  .subscribe(dato=>{
    console.log("vista",dato.response)
  })
}

  }
