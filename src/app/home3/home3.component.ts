import { Component, OnInit,Input } from '@angular/core';
import { AppService } from 'src/app/service/app.service'
import { rutasusuario, Rutas } from '../service/interface/interface-class.model';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-home3',
  templateUrl: './home3.component.html',
  styleUrls: ['./home3.component.css']
})
export class Home3Component implements OnInit {

  idPasajerol: any;
  datasr : any;
  datasc: any;
  rutas:Array<rutasusuario>;
  idRuta: any;
  codigo: any;
  rutaseleccion2: any;
  idmodal: any;

  @Input() idPasajero: string;
  constructor(private router: Router, private service: AppService) { }

  ngOnInit() {
  this.idPasajero = JSON.parse(localStorage.getItem('Datos usuario')).idPasajero;
  this.idRuta = JSON.parse(localStorage.getItem('Datos ruta')) as Array<Rutas>;
  this.local();
 this.rutas = new Array<rutasusuario>();
  }
  rutaActual(idRuta){
    let rutaseleccion = this.rutas.filter((ruta) => ruta.idRuta == idRuta);
    localStorage.setItem('Datos ruta',JSON.stringify(rutaseleccion));
    console.log("Vista:",rutaseleccion[0].idRuta);
  }
  local(){
    this.service.postMisrutas(this.idPasajero)
    .subscribe(datasr=>{
      if(datasr.error != 1){
      this.rutas = datasr.response;
    console.log("ruta",this.rutas)
      }else{
        alert("No hay rutas guardadas");
      }    
    }

    )
  }

  modal(idRuta){
    document.getElementById('modal').style.display = 'block';
    let rutaseleccion = this.rutas.filter((ruta) => ruta.idRuta == idRuta);
    localStorage.setItem('Datos ruta',JSON.stringify(rutaseleccion));
    this.idRuta = JSON.parse(localStorage.getItem('Datos ruta')) as Array<Rutas>;
  }

  modal2(){
    document.getElementById('modal').style.display = 'none';
  }

  check(){
    this.rutaseleccion2 = this.rutas.filter((ruta) => ruta.idRuta);
    localStorage.setItem('Datos ruta',JSON.stringify(this.rutaseleccion2));
    this.service.postCheck(this.idPasajero,this.rutaseleccion2[0].idRuta)
    .subscribe(datasc=>{
      if(datasc.error != 1){
        document.getElementById('modal').style.display = 'none';
      console.log(datasc.response)
        }else{
          alert("Hay un error");
        }  
    })
  }
  
}
