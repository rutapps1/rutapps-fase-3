import { Component, OnInit,Input } from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppService } from 'src/app/service/app.service'
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-terminosycondiciones',
  templateUrl: './terminosycondiciones.component.html',
  styleUrls: ['./terminosycondiciones.component.css']
})
export class TerminosycondicionesComponent implements OnInit {
  @Input() idRuta: string; 
  constructor(private _route: ActivatedRoute, private router: Router, private service: AppService, private http: HttpClient) { }

  ngOnInit(): void {
    this.idRuta = this._route.snapshot.paramMap.get("idRuta");
  }

}
