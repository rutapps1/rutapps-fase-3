import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { Formulario1Component } from './formulario1/formulario1.component';
import { Formulario2Component } from './formulario2/formulario2.component';
import { TerminosycondicionesComponent } from './terminosycondiciones/terminosycondiciones.component';
import { GraciasComponent } from './gracias/gracias.component';
import { ConductorComponent } from './conductor/conductor.component';
import { PasajeroComponent } from './pasajero/pasajero.component';
import { ErrorComponent } from './error/error.component';
import { HomeComponent } from './home/home.component';
import { Home2Component } from './home2/home2.component';
import { Home3Component } from './home3/home3.component';
import { RutasComponent } from './rutas/rutas.component';
import { DetalleRutaComponent } from './detalle-ruta/detalle-ruta.component';
import { NovedadesComponent } from './novedades/novedades.component';
import { PruebacontactoComponent } from './pruebacontacto/pruebacontacto.component';

const routes: Routes = [
  { path: 'PruebacontactoComponent', component: PruebacontactoComponent},
  { path: 'Formulario1Component', component: Formulario1Component },
  { path: 'Formulario2Component', component: Formulario2Component },
  { path: '', component: HomeComponent },
  { path: 'HomeComponent', component: HomeComponent },
  { path: 'Home2Component', component: Home2Component },
  { path: 'Home3Component', component: Home3Component },
  { path: 'RutasComponent', component: RutasComponent },
  { path: 'DetalleRutaComponent', component: DetalleRutaComponent },
  { path: 'NovedadesComponent', component: NovedadesComponent  },
  { path: 'TerminosycondicionesComponent', component: TerminosycondicionesComponent },
  { path: 'GraciasComponent', component: GraciasComponent},
  { path: 'ConductorComponent', component: ConductorComponent},
  { path: 'PasajeroComponent', component: PasajeroComponent},
  { path: '**', component:ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }