import { Component, OnInit,Input  } from '@angular/core';
import { AppService } from 'src/app/service/app.service';
import { Rutas } from '../service/interface/interface-class.model';

@Component({
  selector: 'app-rutas',
  templateUrl: './rutas.component.html',
  styleUrls: ['./rutas.component.css']
})
export class RutasComponent implements OnInit {

  idEmpresal: any;
  dato: any;
  rutas:Array<Rutas>;
  @Input() idEmpresa: string;
  idRuta: string;

  constructor(private service: AppService) { }

  ngOnInit(){
    this.idEmpresa = JSON.parse(localStorage.getItem('Datos usuario')).idEmpresa;
    this.rutasempresa();
    this.rutas = new Array<Rutas>();
  }

  rutaActual(idRuta){
    let rutaseleccion = this.rutas.filter((ruta) => ruta.idRuta == idRuta);
    localStorage.setItem('Datos ruta 2',JSON.stringify(rutaseleccion));
    console.log("Vista:",rutaseleccion);
  }

  rutasempresa(){
this.service.postRutasempresa(this.idEmpresa)
.subscribe(dato=>{
  if(dato.error != 1){
    this.rutas = dato.response;
    console.log(this.rutas)
      }else{
        alert(dato.response);
      }  
}
)
  }

}
