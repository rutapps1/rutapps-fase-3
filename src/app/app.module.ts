import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { QRComponent } from './qr/qr.component';
import { Formulario1Component } from './formulario1/formulario1.component';
import { Formulario2Component} from './formulario2/formulario2.component';
import { TerminosycondicionesComponent } from './terminosycondiciones/terminosycondiciones.component';
import { GraciasComponent } from './gracias/gracias.component';
import { ConductorComponent } from './conductor/conductor.component';
import { PasajeroComponent } from './pasajero/pasajero.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BlockUIModule } from 'ng-block-ui';
// firebase
import { environment } from 'src/environments/environment.prod';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from '@angular/common/http';
import { ErrorComponent } from './error/error.component';
import { HomeComponent } from './home/home.component';
import { Home2Component } from './home2/home2.component';
import { Home3Component } from './home3/home3.component';
import { RutasComponent } from './rutas/rutas.component';
import { DetalleRutaComponent } from './detalle-ruta/detalle-ruta.component';
import { NovedadesComponent } from './novedades/novedades.component';
import { PruebacontactoComponent } from './pruebacontacto/pruebacontacto.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    QRComponent,
    Formulario1Component,
    Formulario2Component,
    TerminosycondicionesComponent,
    GraciasComponent,
    ConductorComponent,
    PasajeroComponent,
    ErrorComponent,
    HomeComponent,
    Home2Component,
    Home3Component,
    RutasComponent,
    DetalleRutaComponent,
    NovedadesComponent,
    PruebacontactoComponent
  ],
  exports:[MatDialogModule],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule, 
    ReactiveFormsModule,
    BlockUIModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
