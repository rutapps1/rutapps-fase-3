// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCGyvNwbe_blTvDLc9_fj3pGv3sFC8i5Ig",
    authDomain: "rutapps-183a3.firebaseapp.com",
    databaseURL: "https://rutapps-183a3.firebaseio.com",
    projectId: "rutapps-183a3",
    storageBucket: "rutapps-183a3.appspot.com",
    messagingSenderId: "228222983194",
    appId: "1:228222983194:web:0f3bee77c567f36b1c05a3",
    measurementId: "G-5J3YGCNP6H"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
